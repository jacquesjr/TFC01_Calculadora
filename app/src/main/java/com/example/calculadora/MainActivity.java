package com.example.calculadora;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    TextView historico, resultado;
    boolean primeiro = false;
    double valor1, valor2, resultadoFinal;
    int ultimoValor = 0; //Qual o tipo do ultimo valor escolhido na calculadora; 0 = numero/nada, 1 = igual, 2 = operador;
    String operador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        historico = (TextView) findViewById(R.id.historico);
        resultado = (TextView) findViewById(R.id.resultado);
        resultado.setText("0");
        historico.setText(" ");
    }

    public void OnClickButtonNumero(View view){
        if(ultimoValor == 2){
            this.resultado.setText("0");
            ultimoValor = 0;
        }
        if(ultimoValor == 1){
            this.historico.setText("");
            ultimoValor = 0;
        }
        Button btn = (Button) view;
        String numero = btn.getText().toString();
        String numerosAnteriores = "";

        if(btn.getId() != R.id.btnVirgula) {
            if (this.resultado.getText().toString() != "0") {
                numerosAnteriores = this.resultado.getText().toString();
                this.resultado.setText(numerosAnteriores + numero);
            }else{
                this.resultado.setText(numero);
            }
        }else if(!(resultado.getText().toString().contains("."))){
            numerosAnteriores = this.resultado.getText().toString();
            this.resultado.setText(numerosAnteriores + numero);
        }
    }

    public void OnClickBtnApagar(View view){
        if(ultimoValor != 2){
            String stringResultado = resultado.getText().toString();
            if(stringResultado.length() > 1) {
                this.resultado.setText(stringResultado.substring(0, stringResultado.length() - 1));
            }else if(stringResultado.length()==1){
                if(stringResultado != "0") {
                    this.resultado.setText("0");
                }
            }
        }
    }

    public void OnClickBtnC(View view){
        ultimoValor = 0;
        primeiro = false;
        this.resultado.setText("0");
        this.historico.setText("");
    }

    public void onClickBtnCE(View view){
        if(ultimoValor == 1){
            this.resultado.setText("0");
            this.historico.setText("");
            ultimoValor = 0;
        }else if(ultimoValor == 2){
            this.resultado.setText("0");
            ultimoValor = 0;
        }else{
            this.resultado.setText("0");
        }
    }

    public void onClickBtnOperador(View view){
        String rResult = resultado.getText().toString();
        if(ultimoValor == 2){
            Button button = (Button) view;
            String sString = historico.getText().toString();
            String aux = sString.substring(0, sString.length() -2);
            sString = aux + button.getText().toString() + " ";
            this.historico.setText(sString);
            operador = button.getText().toString();
        }else{
            if(ultimoValor == 1){
                ultimoValor = 0;
                this.historico.setText("");
            }
            Button btn = (Button) view;
            String numero = " " + btn.getText().toString() + " ";
            if(!primeiro){
                operador = btn.getText().toString();
                resultadoFinal = Double.parseDouble(resultado.getText().toString());
                primeiro = true;
                this.resultado.setText(convertDouble(resultadoFinal));
            } else if(primeiro){
                valor1 = resultadoFinal;
                valor2 = Double.parseDouble(resultado.getText().toString());
                switch (operador) {
                    case "+":
                        resultadoFinal = valor1 + valor2;
                        break;

                    case "-":
                        resultadoFinal = valor1 - valor2;
                        break;

                    case "÷":
                        resultadoFinal = valor1 / valor2;
                        break;

                    case "×":
                        resultadoFinal = valor1 * valor2;
                        break;
                }
                this.resultado.setText(convertDouble(resultadoFinal));
                operador = btn.getText().toString();
            }

            String histFinal = this.historico.getText().toString();
            this.historico.setText(histFinal + rResult + numero);
            this.ultimoValor = 2;
            operador = btn.getText().toString();
        }
    }

    String convertDouble (Double duplo){
        String valor = Double.toString(duplo);
        StringBuilder doisUltimosCharacteres = new StringBuilder();
        doisUltimosCharacteres.append(valor.charAt(valor.length() -2 ));
        doisUltimosCharacteres.append(valor.charAt(valor.length() -1));
        if(doisUltimosCharacteres.toString().equals(".0")){
            return valor.substring(0, valor.length() -2);
        }
        return valor;
    }

    public void OnClickBtnIgual(View view){
        String rResultado = resultado.getText().toString();
        Button btn = (Button) view;
        String texto = " " + btn.getText().toString() + " ";
        if(ultimoValor != 1) {
            if (primeiro) {
                valor1 = resultadoFinal;
                valor2 = Double.parseDouble(resultado.getText().toString());
                switch (operador) {
                    case "+":
                        resultadoFinal = valor1 + valor2;
                        break;

                    case "-":
                        resultadoFinal = valor1 - valor2;
                        break;

                    case "÷":
                        resultadoFinal = valor1 / valor2;
                        break;

                    case "×":
                        resultadoFinal = valor1 * valor2;
                        break;
                }
                ultimoValor = 1;
                String aux3 = this.historico.getText().toString();
                this.historico.setText(aux3 + rResultado + texto);
                this.resultado.setText(convertDouble(resultadoFinal));
                primeiro = false;
            }
        } else {
            switch (operador) {
                case "+":
                    resultadoFinal = resultadoFinal + valor2;
                    break;

                case "-":
                    resultadoFinal = resultadoFinal - valor2;
                    break;

                case "÷":
                    resultadoFinal = resultadoFinal / valor2;
                    break;

                case "×":
                    resultadoFinal = resultadoFinal * valor2;
                    break;
            }
            ultimoValor = 1;
            this.historico.setText(resultadoFinal + ' ' + operador + ' ' + valor2);
            this.resultado.setText(convertDouble(resultadoFinal));
        }
    }
}
